/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab1_homework.wiewióra;

import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Homework1 extends javax.swing.JFrame {

    private static final Logger logger = Logger.getLogger(Homework1.class.getName());
    public Homework1() {
        initComponents();
        addWindowListener(new WindowAdapter(){
           public void Disconnect(WindowEvent e){
                try 
                {            
                    DbManager.Disconnect();        
                } 
                catch (SQLException ex) 
                {
                    logger.log(Level.SEVERE, null, ex);  
                }
           } 
        });
        try 
        {            
            if(!DbManager.Connect())           
                logger.log(Level.SEVERE, null,"Błąd połączenia");  
        } 
        catch (ClassNotFoundException | SQLException ex) 
        { 
            logger.log(Level.SEVERE, null,ex);
        }
        LoadData();    
    }

    private void LoadData(){
        try 
        {            
            ArrayList<DataRecord> records = DbManager.getData();
            Object[][] data = new Object[records.size()][];
            for(int i = 0; i < records.size(); i++)
            {
                data[i] = records.get(i).ToObject();
            }
            DataTable.setModel(GetDataModel(data));
            
        } 
        catch (SQLException ex) 
        {
            logger.log(Level.SEVERE, null,ex); 
        }
        
    }
    
    private DefaultTableModel GetDataModel(Object[][] data){        
        if(data == null)
            data = new Object [][] {{null, null, null, null, null, null, null, null}};
        return new DefaultTableModel(
            data,
            new String [] {
                "Id", "Brand", "Material", "Type", "Color", "Lining", "Bars", "ToDelete"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, 
                java.lang.String.class, 
                java.lang.String.class, 
                java.lang.String.class, 
                java.lang.String.class, 
                java.lang.Boolean.class, 
                java.lang.Boolean.class,
                java.lang.Boolean.class
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        };
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        DataTable = new javax.swing.JTable();
        RefreshButton = new javax.swing.JButton();
        NewRecordButton = new javax.swing.JButton();
        UpdateButton = new javax.swing.JButton();
        DeleteButton = new javax.swing.JButton();
        BrandField = new javax.swing.JTextField();
        MaterialField = new javax.swing.JTextField();
        TypeField = new javax.swing.JTextField();
        ColorField = new javax.swing.JTextField();
        LiningField = new javax.swing.JCheckBox();
        BarsField = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        DataTable.setModel(GetDataModel(null));
        jScrollPane1.setViewportView(DataTable);

        RefreshButton.setText("Refresh");
        RefreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshButtonActionPerformed(evt);
            }
        });

        NewRecordButton.setText(" Add Record");
        NewRecordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewRecordButtonActionPerformed(evt);
            }
        });

        UpdateButton.setText("Update");
        UpdateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateButtonActionPerformed(evt);
            }
        });

        DeleteButton.setText("Delete");
        DeleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteButtonActionPerformed(evt);
            }
        });

        BrandField.setText("Brand");

        MaterialField.setText("Material");
        MaterialField.setToolTipText("");

        TypeField.setText("Type");
        TypeField.setToolTipText("");

        ColorField.setText("Color");

        LiningField.setText("Lining");
        LiningField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LeatherFieldActionPerformed(evt);
            }
        });

        BarsField.setText("Laces");
        BarsField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LacesFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(UpdateButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RefreshButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(DeleteButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(BrandField, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ColorField, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(MaterialField, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LiningField, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TypeField, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BarsField, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(NewRecordButton, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BrandField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(MaterialField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TypeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LiningField)
                            .addComponent(BarsField)
                            .addComponent(ColorField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(NewRecordButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DeleteButton)
                    .addComponent(UpdateButton)
                    .addComponent(RefreshButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void UpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        try
        {
            TableModel model = DataTable.getModel();
            for(int i =0; i < model.getRowCount(); i++)
            {
                DataRecord record = new DataRecord(model, i);
                if(!record.Validate())
                {
                    showMessageDialog(this, "One or more values are incorrect");
                    return;
                } 
                DbManager.Update(record);
            }
            LoadData();
        }
        catch(HeadlessException | SQLException ex)
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }                                            

    private void NewRecordButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                
        try
        {
            DataRecord record = new DataRecord(this);
            if(!record.Validate())
            {
                showMessageDialog(this, "One or more values are incorrect");
                return;
            }               
            DbManager.Add(record);
            LoadData();
        }
        catch(HeadlessException | SQLException ex)
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }                                               

    private void RefreshButtonActionPerformed(java.awt.event.ActionEvent evt) {                                              
        LoadData();
    }                                             

    private void LacesFieldActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
    }                                          

    private void LeatherFieldActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
    }                                            

    private void DeleteButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        try
        {
            TableModel model = DataTable.getModel();
            for(int i =0; i < model.getRowCount(); i++)
            {
                if((boolean)model.getValueAt(i, 7))
                {
                    DbManager.Delete((int) model.getValueAt(i, 0));
                }
            }
            LoadData();
        }
        catch(Exception ex)
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }                                            

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Homework1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Homework1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Homework1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Homework1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Homework1().setVisible(true);
        });
    }
    
    public javax.swing.JTextField BrandField;             
    public javax.swing.JTextField MaterialField;
    public javax.swing.JTextField TypeField;
    public javax.swing.JTextField ColorField;
    public javax.swing.JCheckBox LiningField;
    public javax.swing.JCheckBox BarsField;
    
    
    private javax.swing.JTable DataTable;
    private javax.swing.JButton DeleteButton;
    private javax.swing.JButton NewRecordButton;
    private javax.swing.JButton RefreshButton;  
    private javax.swing.JButton UpdateButton;
    private javax.swing.JScrollPane jScrollPane1;                  
}
