/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab1_homework.wiewióra;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public final class DbManager {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:C:\\Users\\48537\\Documents\\NetBeansProjects\\Lab1_Homework-Wiewióra\\db\\ties";
    
    private static java.sql.Connection conn;
    private DbManager() { }

    public static boolean Connect() throws ClassNotFoundException, SQLException 
    {
        conn = DriverManager.getConnection(JDBC_URL);
        if (conn == null) 
        {
                return false;        
        } 
        else 
        {
                return true;        
        }
    }
    
    public static boolean Disconnect() throws SQLException		
    {
        if (conn == null) 
        {
            return false;        
        } 
        else 
        {            
            conn.close();
            return true;        
        }    
    }
    
    public static ArrayList<DataRecord> getData() throws SQLException 
    {
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery("SELECT * FROM APP.TIES");
        ArrayList<DataRecord> data = new ArrayList<DataRecord>();         
        while (rs.next()) 
        {
            data.add(new DataRecord(rs));
        }        
        stat.close();     
        return data;    
    }  
    
    public static void Delete(Integer id) throws SQLException
    {
        Statement stat = conn.createStatement();
        stat.execute("DELETE FROM APP.TIES WHERE Id = " + id);
        stat.close();  
    }
    
    public static void Add(DataRecord value) throws SQLException
    {
        Statement stat = conn.createStatement();
        stat.execute("INSERT INTO APP.TIES "
            + "(Id, BRAND, MATERIAL, TYPE, COLOR, HAS_LINING, TIE_BARS_INCLUDED) VALUES ("
            + "(SELECT MAX(Id) FROM APP.TIES)+1,"
            + " '"+value.brand
            +"', '"+value.material
            +"', '"+value.type
            +"', '"+value.color
            +"', "+value.lining
            +",  "+value.bars+")");
        stat.close();  
    }
    
    public static void Update(DataRecord value) throws SQLException
    {
        try (Statement stat = conn.createStatement()) {
            stat.execute("UPDATE APP.TIES SET "
                    + "   BRAND = '" + value.brand
                    + "', MATERIAL = '" + value.material
                    + "', TYPE = '" + value.type
                    + "', COLOR = '"+ value.color
                    + "', HAS_LINING = " + value.lining
                    + ",  TIE_BARS_INCLUDED = " + value.bars
                    + " WHERE ID = " + value.Id);
        }  
    }
    
}
