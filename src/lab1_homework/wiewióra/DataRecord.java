/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab1_homework.wiewióra;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableModel;

public class DataRecord {
    int Id;
    String brand;
    String material; 
    String type; 
    String color;
    boolean lining;
    boolean bars;
    
    public boolean Validate() {
        if(brand.length() > 20 || brand.length() == 0) return false;
        if(material.length() > 15 || brand.length() == 0) return false;
        if(type.length() > 10 || brand.length() == 0) return false;
        if(color.length() > 15 || brand.length() == 0) return false;
        return true;
    }
    
    public DataRecord(ResultSet rs) throws SQLException{
        Id = Integer.parseInt(rs.getString(1));
        brand = rs.getString(2);
        material = rs.getString(3);
        type = rs.getString(4);
        color = rs.getString(5);
        lining = rs.getBoolean(6);
        bars = rs.getBoolean(7);
    }
    
    public DataRecord(TableModel model, int i){
        Id = (int) model.getValueAt(i, 0);
        brand = (String)model.getValueAt(i, 1);
        material = (String)model.getValueAt(i, 2);
        type = (String)model.getValueAt(i, 3);
        color = (String)model.getValueAt(i, 4);
        lining = (boolean)model.getValueAt(i, 5);
        bars = (boolean)model.getValueAt(i, 6);
    }
   
    public DataRecord(Homework1 form){
        brand = form.BrandField.getText();
        material = form.MaterialField.getText();
        type = form.TypeField.getText();
        color = form.ColorField.getText();             
        lining = form.LiningField.isSelected();
        bars = form.BarsField.isSelected();
    }
    
    public Object[] ToObject(){
        return new Object[]{
            Id,
            brand,
            material,
            type,
            color,
            lining,
            bars,
            false
        };
    }
}
